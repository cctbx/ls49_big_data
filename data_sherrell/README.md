## On the origin of files in this directory

### Raw metal files
* `Fe.dat`
* `Mn.dat`

These files were downloaded directly from the University of Washington X-ray
Absorption Edges [website](http://skuld.bmsc.washington.edu/scatter/AS_periodic.html)
and saved here as-is, without any further modifications. They accurately
present a general shape of the anomalous contribution to the scattering factors
as a function of energy in a very wide range of 1–25 keV, but are limited
to 5eV resolution around respective absorption edges. As a result, they
generally should be avoided when evaluating or simulating diffraction patterns
around the absorption edge.

### Spliced files
* `Mn2O3_spliced.dat`
* `MnO2_spliced.dat`

These files were spliced from two sources. First, both the real (f'')
and imaginary (f') parts of the anomalous scattering were downloaded from
the University of Washington X-ray Absorption Edges
[website](http://skuld.bmsc.washington.edu/scatter/AS_periodic.html).
In order to increase the resolution around the absorption edge area,
experimental Mn2O3 and MnO2 curves of f'' were received from a correspondence
with Vittal Yachandra. They data back to an article about X-ray damage to the
Mn4Ca complex in PSII ([link](https://doi.org/10.1073/pnas.0505207102)).

The Washington files are expressed in absolute units, whether the curves in
Yachandra files are unitless along the "y" axis. In order to match the latter
to the former, the f'' values in Yachandra files were offset and rescaled,
until a satisfactory visual agreement was found. The Mn2O3 data was offset by
+0.45 units and rescaled by a factor of +3.55. Likewise, the MnO2 data
was offset by +0.45 and rescaled by a factor of 3.31. The data points
within the range of energy covered by the Yachandra files were then removed
from the Washington files and replaced, thus splicing the f'' information.
This process was performed by Daniel Tchoń in March 2023.

Resulting files featured f'' curves which were both complete and accurate
around the edge, but now lacked f' component in the "Yachandra range".
The f' component was consequently calculated numerically based on f''
using Kramers-Kronig relation between the real and imaginary
part of the anomalous scaterring factor.
This process was performed by Vidya Ganapati in April 23.

Resulting files feature a physically-reasonable, although not necessarily
scientifically accurate, values of both f' and f'' in the whole range between
1 and 25 keV, with approximately 20meV resolution. Since the f'' was only
crudely spliced and f' was numerically estimated based on said splice,
this data should be good enough to differentiate between or simulate two
electron states, but should not be overly relied on when comparing to real
experimental curves.
